# Python utility functions for manipulating dislocation network structure
# Translated from matlab functions (Nicolas Bertin) by Wei Cai

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

def pbc_position(r1, r2, L):
    ds = (r2-r1)/L
    ds = ds - ds.round()
    return r1 + ds*L

def plot_structure(rn, links, L, fig=None, ax=None, block=False):
    if fig==None:
        fig = plt.figure(figsize=(8,8))
    if ax==None:
        ax = plt.axes(projection='3d')

    plt.cla()
    for link_id in range(links.shape[0]):
        node_id = links[link_id,:2].astype(int) 
        r_link = rn[node_id,:]
        r_link[1,:] = pbc_position(r_link[0,:], r_link[1,:], L)
        ax.plot(r_link[:,0], r_link[:,1], r_link[:,2], '-b', linewidth=1)

    ax.scatter(rn[:,0], rn[:,1], rn[:,2], c='r', s=4)
    ax.set_xlim(-L/2, L/2)
    ax.set_ylim(-L/2, L/2)
    ax.set_zlim(-L/2, L/2)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.set_aspect('equal')

    plt.draw()
    plt.show(block=block)
    plt.pause(0.01)

    return fig, ax

def plot_stress_density_strain(stress, dens, strain, step, schmid, fig=None, ax1=None, ax2=None, block=False):
    if fig==None:
        fig = plt.figure(figsize=(8,8))
    if ax1==None:
        ax1 = plt.axes()
    if ax2==None:
        ax2 = ax1.twinx() 
    plt.cla()
    ax1.plot(strain[0:step]*100/schmid, stress[0:step]/1e6*schmid, c='b')
    ax2.plot(strain[0:step]*100/schmid, dens[0:step], c='r')

    ax1.set_xlabel('Shear strain (%)')
    ax1.set_ylabel('Shear stress (MPa)')
    ax2.set_ylabel('Density (m^{-2})')
    plt.draw()
    plt.show(block=block)
    plt.pause(0.01)
    return fig, ax1, ax2

def segment_length(n0, n1, rn, L=None):
# Computes length of segment n0-n1
# PBC are applied if L is provided

    r0 = rn[n0,0:3]
    r1 = rn[n1,0:3]
    if not L==None:
        r1 = pbc_position(r0, r1, L)
    return np.linalg.norm(r1-r0)

def all_segments_length(rn, links, L=None):
# Returns lengths of all segments
# PBC are applied if L is provided

    m = links.shape[0]
    segslen = np.zeros([m,1]);
    for i in range(m):
        n0 = links[i,0].astype(int)
        n1 = links[i,1].astype(int)
        if n0>=0 and n1>=0:
            segslen[i] = segment_length(n0, n1, rn, L)
    return segslen


def remove_nodes(rn, links, nodesid):
# Remove nodes from the network
# Nodes and links lists will be updated
# Warning: the conn list will not be updated if it exists!
# Translated from remove_nodes.m (Nicolas Bertin)

    # Remove connections associated
    # with nodes to be removed
    for j in nodesid[:]:
        il = np.where( np.logical_or(links[:,0]==j, links[:,1]==j ) )[0]
        #print(il)
        links = np.delete(links, il, axis=0)

    # Create offset vector
    n = rn.shape[0]
    offset = -np.ones([n,1],dtype=int)
    os = 0
    for i in range(n):
         if (np.array(nodesid) == i).any():
             os = os + 1
             offset[i] = -1
         else:
             offset[i] = i-os

    # Update links list
    for i in range(links.shape[0]):
         links[i,0] = offset[links[i,0].astype(int)]
         links[i,1] = offset[links[i,1].astype(int)]

    # Update rn list
    rn = np.delete(rn, nodesid, axis=0)

    return rn, links, offset

def purge_network(rn, links):
# Purge the network by removing nodes that are unconnected to the network
# Translated from purge_network.m (Nicolas Bertin)

    nodesid = np.unique([links[:,0], links[:,1]])
    nodesid = np.setxor1d(np.arange(rn.shape[0]), nodesid)
    rn, links, offset = remove_nodes(rn, links, nodesid)
    return rn, links, offset

def generate_connectivity(n, links):
# Generates nodal connectivity table corresponding to the link list
# Inputs
#  n: number of nodes (size of the connectivity table)
#  links: list of links

    conn = np.zeros([n,10],dtype=int)
    for i in range(links.shape[0]):
        #print(i)
        n0 = links[i,0].astype(int)
        n1 = links[i,1].astype(int)
        #print(n0)
        #print(n1)
        conn[n0,0] = conn[n0,0] + 1
        conn[n1,0] = conn[n1,0] + 1
        if conn[n0,0] >= conn.shape[1] or conn[n1,0] >= conn.shape[1]:
            conn = np.hstack((conn, np.zeros([conn.shape[0],1])))
        conn[n0,conn[n0,0]] = n1
        conn[n1,conn[n1,0]] = n0

    return conn

def find_segment(n0, n1, links, option='first'):
#Finds segment(s) n0-n1 in the links list and returns its index and 
# the order (1 or -1) of the nodes
# 
# Syntax
#  [s,order] = find_segment(n0,n1,links)
#  [s,order] = find_segment(n0,n1,links,'all')
#
# Inputs
#  n0: id of the first node
#  n1: id of the second node
#  links: list of links
#  option: (optional) finder option, default = 'first'
#          - option = 'first': find first matching link
#          - option = 'all':   find all matching links
#
# Outputs
#  s: id of the segment(s) in the link list
#  order: order of the nodes for segment(s) s (n0-n1 = 1, n1-n0 = -1)

    if option == 'first':
        order = np.array([1]) 
        s = np.where( np.logical_and(links[:,0]==n0, links[:,1]==n1 ) )[0]
        if len(s) == 0:
            s = np.where( np.logical_and(links[:,0]==n1, links[:,1]==n0 ) )[0]
            if len(s) > 0:
                order = np.array([-1])
    else:
        s0 = np.where( np.logical_and(links[:,0]==n0, links[:,1]==n1 ) )[0]
        s1 = np.where( np.logical_and(links[:,0]==n1, links[:,1]==n0 ) )[0]
        s = np.concatenate((s0, s1), axis=None)
        order = np.concatenate((np.ones_like(s0), np.ones_like(s1)*(-1)), axis=None)

    return s, order

def spin_matrix(phi):

    Ss = phi
    Cs = 1 - 0.5*phi**2

    Rspin = np.zeros([3,3])

    Rspin[0,0] =  Cs[2]*Cs[1]
    Rspin[1,1] =  Cs[2]*Cs[0] + Ss[2]*Ss[1]*Ss[0];
    Rspin[2,2] =  Cs[1]*Cs[0];
    Rspin[0,1] = -Ss[2]*Cs[0] + Cs[2]*Ss[0]*Ss[1];
    Rspin[1,2] = -Cs[2]*Ss[0] + Ss[2]*Ss[1]*Cs[0];
    Rspin[2,0] = -Ss[1];
    Rspin[0,2] =  Ss[2]*Ss[0] + Cs[2]*Cs[0]*Ss[1];
    Rspin[1,0] =  Ss[2]*Cs[1];
    Rspin[2,1] =  Cs[1]*Ss[0];

    return Rspin

