import sys, os
import numpy as np
import torch



def Connectiviy_2xnLinks(nodes):
#Returns connectivty of the graph corresponding to nodes dictionary. We need edge_index to be of the COO type with size 2x(2nLink) to ue in Pytorch torch_geomteric
#Hamedakh

    list_keys = list(nodes.keys() )

    totallink = torch.empty((2,0), dtype=torch.long)
    for counter_my_node, my_node_tag in enumerate(list_keys):
        arms_dict = nodes[my_node_tag][1]
        for nbr_tag in arms_dict.keys():
            totallink = torch.cat( (totallink, torch.tensor([[counter_my_node], [list_keys.index(nbr_tag) ]], dtype=torch.long ) ) , dim=1)

    return totallink


def prepareX_config_12x12(nodes):
    nNodes = len(nodes)
    list_keys = list(nodes.keys() )
    iu1 =  np.triu_indices(12)
    #X_config = dok_matrix( (nNodes, 78) )       #78 equals to (144-12)/2+12 as the X_config will be symmetric
    X_config = torch.zeros( (nNodes, 78) )
#    adj, list_keys = adjacency_matrix(phys_nodes)
    for counter_my_node, my_node_tag in enumerate(list_keys):
        arms_dict = nodes[my_node_tag][1]
        Xnode_12x12 = torch.zeros((12,12))
        for counter1, nbr1_tag in enumerate(arms_dict.keys()):
            link1 = arms_dict[nbr1_tag]
            l1 = link1[8]
            sysID1 = link1[6].astype(int)
            if sysID1 == 12:        #For now, we don't care about junctions
                continue
            for counter2, nbr2_tag in enumerate(arms_dict.keys()):
                if counter1 < counter2:
                    continue
                link2 = arms_dict[nbr2_tag]
                l2 = link2[8]
                sysID2 = link2[6].astype(int)
                if sysID2 == 12:        #For now, we don't care about junctions
                    continue

                if l1<10:
                    l1=10
                if l2<10:
                    l2=10

                Xnode_12x12[sysID1, sysID2] += 1/l1**2 + 1/l2**2
        X_config[counter_my_node, :] = Xnode_12x12[iu1]

    return X_config


def prepareX_config_12x1(nodes):
    nNodes = len(nodes)
    list_keys = list(nodes.keys() )
    X_config = torch.zeros( (nNodes, 12) )
    for counter_my_node, my_node_tag in enumerate(list_keys):
        arms_dict = nodes[my_node_tag][1]
        Xnode_12x1 = torch.zeros((1,12))
        for counter1, nbr1_tag in enumerate(arms_dict.keys()):
            link1 = arms_dict[nbr1_tag]
            l1 = link1[8]
            sysID1 = link1[6].astype(int)
            if sysID1 == 12:        #For now, we don't care about junctions
                continue
            if l1<10:
                l1=10
            Xnode_12x1[0,sysID1] += 1/l1
        X_config[counter_my_node, :] = Xnode_12x1

    return X_config




#Sasnity check to confirm implementation of the prepareX_config_12x12 function and ensure every thing is fine with nodes dictionary
def X_config_sanity_check(nodes):
    numNodesCheck = 10                        # 10 of the nodes will be randomly selected to check if the summation of the nbr
                                              #rows will be equal to corresponding row in adjxXconfig
    Xconfig, adj, list_keys = prepareX_config_12x12(nodes)

    Xdense = Xconfig.toarray()
    adjxXconfig = adj*Xconfig
    adjxXconfig_dense = adjxXconfig.toarray()
    idx = dict((value, counter)  for (counter, value) in enumerate(list_keys))

    for i in range(numNodesCheck):
        nodeID = random.randint(0, len(list_keys))
        node_key = list_keys[nodeID]
        node_armsdict = nodes[node_key][1]
        Z= np.zeros((1,78))
        for counter1, nbr1_tag in enumerate(node_armsdict.keys()):
            Z += Xdense[idx[nbr1_tag],:]
        if np.amax( np.absolute(Z-adjxXconfig_dense[nodeID,:]) ) >1e-10:
            sys.exit("adjxXconfig is not equal to summation of nbr values")

    print("X_config_sanity_check passed")
