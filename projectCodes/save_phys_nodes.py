import sys, os, shutil
import numpy as np
import time
import json
import pickle

# In a dislocation graph, there are two types of nodes: discretization and physical nodes. Discretization nodes are only connected to
# two other ndoes on the same slip system. It has been shown that mechanical response depends only on the distribution of physical nodes. For this purpose,
# we load the graph (which is represented by nodes dictionary), then remove the discretization nodes and save the results into phys folder


#sys.path.append('/home/hamed/Documents/codes/GitLab/NetworCh/python/ddd/')
os.getcwd()

#from ddd_util import plot_structure, plot_nodes_dict
from ddd_util import read_nodes, read_nodes_to_dict, dict_to_rn_links
from ddd_util import physical_structure, extract_link_length
from ddd_util import oprec_player_on_dict, sanity_check_nodes_dict

Paths = np.array([[0,0,1, 0,1,1],
                  [0,0,1, 1,1,2],
		          [0,0,1, 1,2,3],
                  [0,0,1, 1,3,6],
                  [0,0,1, 2,3,6],
		          [0,1,1, 1,2,3],
                  [0,1,1, 1,3,6],
		          [1,1,1, 1,1,2],
		          [1,1,1, 1,4,6],
		          [1,1,1, 2,3,6] ] )

nPaths = Paths.shape[0]

for i in range(nPaths):
    path_folder  = "changeOrientation_"+str(Paths[i,0])+str(Paths[i,1])+str(Paths[i,2])+"_"+str(Paths[i,3])+str(Paths[i,4])+str(Paths[i,5])+"_v1/"
    path_dir = '/home/hamed/Documents/mnt/Exos/15um_1e3_changeOrientation/'+path_folder

    for X in range(1,10):
        restarts_dir = path_dir + str(X) +'/restart/'
        phys_str_dir = path_dir + str(X) +'/phys/'

        if os.path.isdir(phys_str_dir):
            shutil.rmtree(phys_str_dir)
        os.mkdir(phys_str_dir)
        os.chdir(phys_str_dir)

        for file in os.listdir(restarts_dir):
            if file.endswith(".data"):
                phys_nodes2= {}
                datafile = file
                rsNum = int( datafile.split(".")[0].split("rs")[1] )
                nodes, bounds = read_nodes_to_dict(restarts_dir + datafile)
                phys_nodes = physical_structure(nodes, bounds)
                
                psfile_name = "ps"+str(rsNum)+".pickle"
                pickle_out = open(psfile_name,"wb")
                pickle.dump(phys_nodes, pickle_out)
                pickle_out.close()
