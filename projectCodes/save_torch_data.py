import sys, os, shutil
import numpy as np
import time
import json
import pickle
import torch
from torch.nn import Parameter
from torch_geometric.nn.conv import MessagePassing
import torch.nn.functional as F
from torch_geometric.nn import GraphConv


#This file, calculates the feature vector of each node, and saves results using torch.save
#This was found to significantly speed up the training, compared with loading the grapha and calculate feature vector of the graph, during training.
# Processing 100 Graphs takes ~6 min on single GPU, if calculateing feature vectors at each iteratinos. 
# Processing 100 Graphs takes ~6 sec on single GPU, if nodes' feature vectors are saved and then loaded using torch.load

#os.chdir('/home/hamed/Documents/mnt/Exos/15um_1e3_changeOrientation/MLmat/DislocationDynamics/SlipActivity/python')
from graphConv_Util import Connectiviy_2xnLinks, prepareX_config_12x1, prepareX_config_12x12

#sys.path.append('/home/hamed/Documents/codes/GitLab/NetworCh/python/ddd/')
os.getcwd()

from ddd_util import read_nodes, read_nodes_to_dict, dict_to_rn_links
from ddd_util import physical_structure, extract_link_length
from ddd_util import oprec_player_on_dict, sanity_check_nodes_dict

Paths = np.array([[0,0,1, 0,1,1],
                  [0,0,1, 1,1,2],
		          [0,0,1, 1,2,3],
                  [0,0,1, 1,3,6],
                  [0,0,1, 2,3,6],
		          [0,1,1, 1,2,3],
                  [0,1,1, 1,3,6],
		          [1,1,1, 1,1,2],
		          [1,1,1, 1,4,6],
		          [1,1,1, 2,3,6] ] )

nPaths = Paths.shape[0]

for i in range(nPaths):
    path_folder  = "changeOrientation_"+str(Paths[i,0])+str(Paths[i,1])+str(Paths[i,2])+"_"+str(Paths[i,3])+str(Paths[i,4])+str(Paths[i,5])+"_v1/"
    path_dir = '/home/hamed/Documents/mnt/Exos/15um_1e3_changeOrientation/'+path_folder

    for X in range(1,10):
        restarts_dir = path_dir + str(X) +'/restart/'
        phys_str_dir = path_dir + str(X) +'/phys/'
        torch_data_dir = path_dir + str(X) +'/torch_data/invLi_12x1'	#change it to torch_data_invSqli_78x1 to save torch_data correposndnig to an input of 12x12 matrix (reduced size of 78)

        if os.path.isdir(torch_data_dir):
            shutil.rmtree(torch_data_dir)
        os.mkdir(torch_data_dir)
        os.chdir(torch_data_dir)

        for file in os.listdir(phys_str_dir):
            data = {}
            rsNum = int( file.split(".")[0].split("ps")[1] )
            ps_fileName = phys_str_dir+file
            pickle_out = open(ps_fileName,"rb")
            phys_nodes = pickle.load(pickle_out)
            pickle_out.close()

            data['x'] = 1e3 *prepareX_config_12x1(phys_nodes)				#change it to prepareX_config_12x12 to save torch_data correposndnig to an input of 12x12 matrix (reduced size of 78)
            data['edge_index'] = Connectiviy_2xnLinks(phys_nodes)

            file_name = torch_data_dir+"/tensor"+str(rsNum)+".torch"
            torch.save({'x': data['x'], 'edge_index': data['edge_index']}, file_name)
