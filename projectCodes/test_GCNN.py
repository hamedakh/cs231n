import sys, os, shutil, datetime
import numpy as np
import time
import json
import pickle
import random
from  scipy.sparse import coo_matrix, dok_matrix, csr_matrix
import torch
from torch.nn import Parameter
from torch_geometric.nn.conv import MessagePassing
import torch.nn.functional as F
from torch_geometric.nn import GraphConv, GATConv
from scipy.io import loadmat
import matplotlib.pyplot as plt

sys.path.append('/home/hamed/Documents/codes/GitLab/NetworCh/python/ddd/')
os.getcwd()
os.chdir('/home/hamed/Documents/mnt/Exos/15um_1e3_changeOrientation')

from ddd_util import read_nodes, read_nodes_to_dict, dict_to_rn_links, dict_to_rn_links, adjacency_matrix
from ddd_util import physical_structure, extract_link_length,  sanity_check_adjMat
from ddd_util import oprec_player_on_dict, sanity_check_nodes_dict

from graphConv_Util import Connectiviy_2xnLinks, prepareX_config_12x1, prepareX_config_12x12

YIELDstrain=0.05e-2


class disloGraphNet(torch.nn.Module):
    def __init__(self):
        super(disloGraphNet, self).__init__()
        self.conv1 = GraphConv (78,4)           #change GraphConv to GATConv in case of GatedGCNN
        self.conv2 = GraphConv (4,4)
        self.conv3 = GraphConv (4,4)
        self.fc1 = torch.nn.Linear(4, 1)

    def forward(self, data):
        x = data['x']
        edge_index = data['edge_index']
        x = self.conv1(x, edge_index)
        x = F.leaky_relu(x, negative_slope=0.1)
        x = self.conv2(x, edge_index)
        x = F.leaky_relu(x, negative_slope=0.1)
        x = self.conv3(x, edge_index)
        x = F.leaky_relu(x, negative_slope=0.1)
        x = torch.mean(x, dim=0)
        x = self.fc1(x)

        return x



DDsimul = loadmat('MLmat/DislocationDynamics/SlipActivity/data/DDsimul_81points.mat')['DDsimul']

cnFreq = 100

Paths = np.array([[0,0,1, 0,1,1],
                  [0,0,1, 1,1,2],
		          [0,0,1, 1,2,3],
                  [0,0,1, 1,3,6],
                  [0,0,1, 2,3,6],
		          [0,1,1, 1,2,3],
                  [0,1,1, 1,3,6],
		          [1,1,1, 1,1,2],
		          [1,1,1, 1,4,6] ] )

                  #[1,1,1, 2,3,6] keep for test

count = 0
nepochs = 5
nSimul = len(Paths)

model = disloGraphNet()

values = torch.load('/home/hamed/Documents/mnt/Exos/15um_1e3_changeOrientation/gcn_GC12x4_GC4x4_GC4x4_Ln4x1')
values
model.load_state_dict(torch.load('/home/hamed/Documents/mnt/Exos/15um_1e3_changeOrientation/gcn_GC78x4_GC4x4_GC4x4_Ln4x1'))
model.eval()
i=0
X=1
count_all = np.zeros((9))
stress_NN_all = []
SigmaEps_str_all = []

for i in [0]:	#range(nSimul):	#not all of the data has been uploaded, only i=0 was uploaded
    for X in [0]:	#range(1,9): 	#not all of the data has been uploaded, only X=1 was uploaded
        data = {}
        count = 0
        path_folder  = "changeOrientation_"+str(Paths[i,0])+str(Paths[i,1])+str(Paths[i,2])+"_"+str(Paths[i,3])+str(Paths[i,4])+str(Paths[i,5])+"_v1/"
        path_dir = '/home/hamed/Documents/mnt/Exos/15um_1e3_changeOrientation/'+path_folder

        restarts_dir = path_dir + str(X) +'/restart/'
        phys_str_dir = path_dir + str(X) +'/phys/'
        prop_dir = path_dir + str(X) +'/properties/'
        torch_data_dir = path_dir + str(X) +'/torch_data/torch_data_invSqli_78x1/'    #change to invLi_12x1/' in case of inputs of size 12

        if not os.path.isdir(phys_str_dir):
            sys.exit("There is no phys folder")

        stress_NN = np.zeros(len(os.listdir(phys_str_dir)) )
        for file in os.listdir(phys_str_dir):
            file
            rsNum = int( file.split(".")[0].split("ps")[1] )

            sigma_eps_file = open( prop_dir + 'stress_Total_strain', "r")
            SigmaEps_str = sigma_eps_file.read().split('\n')
            sigma_eps_file.close()
            step_strain =  float(SigmaEps_str[rsNum* cnFreq-1].split(' ')[0])

            if step_strain < YIELDstrain:
                continue
                count +=1
            step_stress = torch.tensor( float(SigmaEps_str[rsNum* cnFreq-1].split(' ')[1])/1e6 - 20 )

            os.chdir(phys_str_dir)

            torch_data_file = torch_data_dir + "tensor" + str(rsNum) + ".torch"
            tensors = torch.load(torch_data_file)
            data['x'] = tensors['x']
            data['edge_index'] = tensors['edge_index']

            stress_NN[count] = model(data) + 20
            print(count)
            count +=1
        count_all[X]=count
        stress_NN_all.append(stress_NN)
        SigmaEps_str_all.append(SigmaEps_str)

for X in [1]: 	#range(1,9):
    SigmaEps_str = SigmaEps_str_all[X-1]
    stress_NN = stress_NN_all[X-1]

    stress_DD = np.zeros((count,1))
    strain_DD = np.zeros((count,1))
    for rsNum in range(1,count):
        stress_DD[rsNum] = float(SigmaEps_str[rsNum* cnFreq-1].split(' ')[1])/1e6
        strain_DD[rsNum] = float(SigmaEps_str[rsNum* cnFreq-1].split(' ')[0])*1e2
    plt.plot(strain_DD[0:count], stress_NN[0:count],'--b')
    plt.plot(strain_DD[0:count], stress_DD[0:count],'r')
    plt.legend(['GNN','DDsimulation'])
    plt.ylabel( 'Flow Stress (MPa)' ).set_fontsize(15)
    plt.xlabel( 'Axial Strain (%)' ).set_fontsize(13)
    plt.title('i= %d' %i +' gcn_GC78x4_GC4x4_GC4x4_Ln4x1')   #i= %d' %i +',X=%d' %X +'_
    plt.ylim([0,40])
os.chdir(path_dir)
plt.savefig('i= %d' %i +'gcn_GC78x4_GC4x4_GC4x4_Ln4x1')


#Following plots the output of each layer. 
import collections
from functools import partial
activations = collections.defaultdict(list)
def save_activation(name, mod, inp, out):
    activations[name].append(out.cpu())


for name, m in model.named_modules():
    m.register_forward_hook( partial(save_activation, name) )

out = model(data)
activations = {name: torch.cat(outputs, 0) for name, outputs in activations.items() }


count=0
plt.figure()
for k, v in activations.items():
    print(k, v.size())
    count+=1
    if count == 5:
        print( np.mean( v.detach().numpy() , axis=0) )
        plt.plot(v[:,0].detach().numpy(),label = r'Avg $\approx -19.3$' )
        plt.plot(v[:,1].detach().numpy(),label = r'Avg $\approx= -28.8$' )
        plt.plot(v[:,2].detach().numpy(),label = r'Avg $\approx= -14.86$' )
        plt.plot(v[:,3].detach().numpy(),label = r'Avg $\approx= -32.81$' )
plt.legend()
plt.title('GraphConv model_gcn_GC12x4_GC4x4_GC4x4_Ln4x1')
plt.xlabel('Edge ID')
plt.ylabel('Activation')
plt.savefig('Activations_gcn_GC12x4_GC4x4_GC4x4_Ln4x1')

for W in model.parameters():
    print(W.data)

params_total = sum(p.numel() for p in model.parameters() )
print(params_total)


