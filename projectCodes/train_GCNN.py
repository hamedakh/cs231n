import sys, os, shutil, datetime
import numpy as np
import time
import json
import pickle
import random
from  scipy.sparse import coo_matrix, dok_matrix, csr_matrix

print('loading torch')
import torch
from torch.nn import Parameter
from torch_geometric.nn.conv import MessagePassing
import torch.nn.functional as F
from torch_geometric.nn import GraphConv
from scipy.io import loadmat
print('torch loaded')

from graphConv_Util import Connectiviy_2xnLinks, prepareX_config_12x1, prepareX_config_12x12

YIELDstrain=0.05e-2



class disloGraphNet(torch.nn.Module):
    def __init__(self):
        super(disloGraphNet, self).__init__()
        self.conv1 = GraphConv (12,4)			  #change GraphConv to GATConv in case of GatedGCNN
        self.conv2 = GraphConv (4,4)
        self.conv3 = GraphConv (4,4)
        self.fc1 = torch.nn.Linear(4, 1)

    def forward(self, data):
        x = data['x']
        edge_index = data['edge_index']
        x = self.conv1(x, edge_index)
        x = F.leaky_relu(x, negative_slope=0.1)
        x = self.conv2(x, edge_index)
        x = F.leaky_relu(x, negative_slope=0.1)
        x = self.conv3(x, edge_index)
        x = F.leaky_relu(x, negative_slope=0.1)
        x = torch.mean(x, dim=0)
        x = self.fc1(x)

        return x



DDsimul = loadmat('DDsimul.mat')['DDsimul']

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu' )
if torch.cuda.is_available():
	print("GPU!")
else:
	print("CPU!")

model = disloGraphNet().to(device)

optimizer = torch.optim.Adam(model.parameters(), lr = 0.05 )
cnFreq = 100
model.train()


Paths = np.array([[0,0,1, 1,1,2],
		          [0,0,1, 1,2,3],
                  [0,1,1, 1,2,3],
                 # [1,1,1, 1,1,2],
                 # [1,1,1, 1,4,6],
                 # [1,1,1, 2,3,6],
                  [0,1,1, 1,3,6],
                  [0,0,1, 2,3,6],
                  [0,0,1, 0,1,1],
                  [0,0,1, 1,3,6] ] )

                  #[1,1,1, 2,3,6] keep for test

count = 0
nepochs = 500



for epoch in range(nepochs):
    count = 0
    processedX = set([])
    while count < 6000:
        data = {}
        i = random.choice( range(len(Paths)) )
        X = random.choice( range(1,10) )
        simID = i*9+(X-1)

        path_folder  = "changeOrientation_"+str(Paths[i,0])+str(Paths[i,1])+str(Paths[i,2])+"_"+str(Paths[i,3])+str(Paths[i,4])+str(Paths[i,5])+"_v1/"
        path_dir = '/oak/stanford/groups/caiwei/hamedakh/15um_1e3_changeOrientation/torch_data/invLi_12x1/'+path_folder


        torch_data_dir = path_dir + str(X) +'/invLi_12x1/'

        if not os.path.isdir(torch_data_dir):
            sys.exit("There is no torch_data_dir")

        all_torchFiles = os.listdir(torch_data_dir)
        rsNum_freq100 = random.choice( range(1, len(all_torchFiles)-1 ))
        file = all_torchFiles[rsNum_freq100-1]

        if (i,X,rsNum_freq100) in processedX:
            continue                #this assures no single data point will be seen twice in one epoch
        else:
            processedX.add( (i,X,rsNum_freq100) )

        #rsNum_freq100 = int( file.split(".")[0].split("tensor")[1] )


        step_strain = DDsimul['axialStrainFreq100'][0,simID][rsNum_freq100 -1 ]

        if step_strain < YIELDstrain:
            continue					# we won't use data points proir to yield of the material for training


        step_stress = torch.tensor( float( DDsimul['axialStressFreq100'][0,simID][rsNum_freq100 - 1]/1e6 - 20.0) ).to(device)

		if (0):				# This calculates nodes feature vector during training
			os.chdir(phys_str_dir)

			pickle_out = open(file,"rb")
			phys_nodes = pickle.load(pickle_out)
			pickle_out.close()
			data['x'] = 1e7 *prepareX_config(phys_nodes).to(device)
			data['edge_index'] = Connectiviy_2xnLinks(phys_nodes).to(device)
			torch_data_file = torch_data_dir + "tensor" + str(rsNum_freq100) + ".torch"
		else:			# This only loads pre-calculated nodes feature vectors
			torch_data_file = torch_data_dir + file
			tensors = torch.load(torch_data_file)
			data['x'] = tensors['x'].to(device)
			data['edge_index'] = tensors['edge_index'].to(device)

        optimizer.zero_grad()
        out = model(data)

        print(out.device)	#To make sure we are using GPU

        loss = F.mse_loss(out, step_stress)
        loss.backward()
        optimizer.step()

        if (count % 100 == 1):
            #print('count = %d' %count+', loss = %f' %loss+', time: %s' %datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
            print('count = %d' %count+', time: %s' %datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

        if (count % 1000 == 1):
            torch.save(model.state_dict(), '/oak/stanford/groups/caiwei/hamedakh/15um_1e3_changeOrientation/gcn_GC12x4_GC4x4_GC4x4_Ln4x1')


        count += 1

for W in model.parameters():
    print(W.data)

params_total = sum(p.numel() for p in model.parameters() )
print(params_total)

#X_config_sanity_check(phys_nodes)

